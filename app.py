#!/usr/bin/env python3

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('WebKit2', '4.0')
from gi.repository import Gtk

from knowledge_model import KnowledgeModel
from viewers import NotesEditor, NotesViewer
from comm_nexus import Nexus
from graph import Graph
from searchbox import SearchBox

if __name__ == "__main__":
    comm_nexus = Nexus()

    knowledge_model = KnowledgeModel()
    knowledge_model.register_nexus(comm_nexus)

    graph = Graph(knowledge_model)
    graph.register_nexus(comm_nexus)
    graph.set_default_size(800, 600)
    graph.connect("destroy", Gtk.main_quit)

    searchbox = SearchBox(knowledge_model)
    searchbox.register_nexus(comm_nexus)
    searchbox.set_default_size(400, 300)
    searchbox.connect("destroy", Gtk.main_quit)

    note_editor = NotesEditor(knowledge_model, 'selected')
    note_editor.register_nexus(comm_nexus)
    note_editor.set_default_size(800, 600)
    note_editor.connect("destroy", Gtk.main_quit)

    note_viewer = NotesViewer(knowledge_model, 'previewed')
    note_viewer.register_nexus(comm_nexus)
    note_viewer.set_default_size(800, 600)
    note_viewer.connect("destroy", Gtk.main_quit)

    comm_nexus.register('select_channel', note_editor)
    comm_nexus.register('save_channel', note_editor)
    comm_nexus.register('preview_channel', note_viewer)
    comm_nexus.register('save_channel', knowledge_model)
    comm_nexus.register('preview_channel', searchbox)
    comm_nexus.register('select_channel', searchbox)
    comm_nexus.register('preview_channel', graph)
    comm_nexus.register('select_channel', graph)

    # this stuff, as well as set_default_size
    # and connect destroy can be called in 
    # __init__ of respective objects
    graph.show_all()
    searchbox.show_all()
    note_editor.show_all()
    note_viewer.show_all()

    # window.load_note('bag of ideas for the system.md')

    Gtk.main()
