#!/usr/bin/env python3

import os
import markdown

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('WebKit2', '4.0')
from gi.repository import Gtk, WebKit2
from gi.repository import Gdk
from gi.repository import GLib
from gi.repository import GObject
from gi.repository import GtkSource

from data_utils import item_types
from other_utils import debug_logger


# settings for gtk
#settings = gtk.Settings.get_default()
#settings.set_property("gtk-theme-name", "Adwaita-dark")
#settings.set_property("gtk-application-prefer-dark-theme", True)

# settings for webkit
#settings = webview.get_settings()
#settings.set_allow_file_access_from_file_urls(True)
#settings.set_allow_universal_access_from_file_urls(True)
#settings.set_auto_load_images(True)
#webview.set_settings(settings)


class CustomCompletionProvider(GObject.GObject, GtkSource.CompletionProvider):
    '''
    code adapted from: https://warroom.rsmus.com/do-that-auto-complete/

    lack of __init__ because of some weird errors, needed a workaround
    '''
    def do_get_name(self):
        return self.name

    def do_match(self, context):
        # this should evaluate the context to determine if this completion
        # provider is applicable, for debugging always return True

        self.process_context(context)

        if self.left_text == '':
            return False
        elif self.matching_items == []:
            return False
        else:
            return True

    def do_populate(self, context):
        '''  '''
        proposals = [
            GtkSource.CompletionItem(
                label=i,
                text=i,
            ) for i in
            self.matching_items
        ]

        context.add_proposals(self, proposals, True)

    def process_context(self, context):
        ''' get relevant text from context '''

        self.left_text = ''

        # found difference in Gtk Versions
        end_iter = context.get_iter()
        if not isinstance(end_iter, Gtk.TextIter):
            _, end_iter = context.get_iter()

        if end_iter:
            buf = end_iter.get_buffer()
            mov_iter = end_iter.copy()

            #
            # provide search results only after last '[['
            # unless there was any of the special characters
            # after it (i.e. newline, space, comma, ], etc)
            #
            search_res = mov_iter.backward_search(
                '[[', Gtk.TextSearchFlags.VISIBLE_ONLY
            )

            if search_res:
                self.left_text = buf.get_text(search_res[0], end_iter, True)

                for m in self.break_on:
                    if m in self.left_text:
                        self.left_text = ''

        #
        # match known items
        #
        self.matching_items = [
            i for i in self.items if
            i.startswith(self.left_text[2:])
        ]


class NotesEditor(Gtk.Window):
    '''
    Window for displaying and editing notes.
    
    In display mode notes are rendered (markdown->html),
    while in editing mode they are shown as raw text.
    '''

    def __init__(self, knowledge_model, title):
        super().__init__(title=title)

        self.knowledge_model = knowledge_model
        self.comm_nexus = None
        self.current_note = None

        vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.add(vbox)

        # init stack
        stack = Gtk.Stack()
        stack.set_transition_type(Gtk.StackTransitionType.NONE)
        stack.set_transition_duration(0)

        # webview - displaying html (markdown)
        self.webview = WebKit2.WebView()

        # editor - displaying raw text
        # using sourceview to support completion
        self.editor = GtkSource.View()
        self.editor.set_editable(True)
        self.buffer = self.editor.get_buffer()
        self.completion = self.editor.get_completion()
        self.prep_autocomplete()

        # add stacks
        stack.add_titled(self.webview, "read", "Read")
        stack.add_titled(self.editor, "edit", "Edit")

        stack_switcher = Gtk.StackSwitcher()
        stack_switcher.set_stack(stack)

        self.save_button = Gtk.Button(label="save")
        self.save_button.connect("clicked", self.save_note)

        self.header = Gtk.HeaderBar()
        self.header.add(stack_switcher)
        self.header.add(self.save_button)

        vbox.pack_start(self.header, False, False, 0)
        vbox.pack_start(stack, False, False, 0)


    def prep_autocomplete(self):
        break_on = [']', ',', ';', ' ', '\n']
        for t in item_types:
            items = self.knowledge_model.filter_item_type([t])

            ids_provider = CustomCompletionProvider()
            # an ugly workaround around bugs which
            # appear if __init__ is used in the
            # CustomCompletionProvider class
            ids_provider.name = t
            ids_provider.items = items
            ids_provider.break_on = break_on

            self.completion.add_provider(ids_provider)

    @debug_logger
    def register_nexus(self, comm_nexus):
        self.comm_nexus = comm_nexus

    @debug_logger
    def load_note(self, note_name):
        """  """
        self.current_note = note_name

        self.load_viewer(note_name)
        self.load_editor(note_name)

    def load_viewer(self, note_name):
        self.webview.load_html(
            self.knowledge_model.get_note(note_name, 'html'),
            'file://%s' % os.path.dirname(os.path.realpath(__file__)),
        )

    def load_editor(self, note_name):
        self.buffer.set_text(
            self.knowledge_model.get_note(note_name, 'markdown')
        )

    @debug_logger
    def save_note(self, _):
        txt = self.buffer.get_text(
            self.buffer.get_start_iter(),
            self.buffer.get_end_iter(),
            True
        )

        self.comm_nexus.on_message(
            'save_channel',
            {
                'type':'save_note',
                'note_name':self.current_note,
                'new_txt':txt.splitlines(keepends=True),
            },
        )

    @debug_logger
    def on_message(self, message):
        if message['type'] == 'select':
            self.load_note(message['note_name'])
        elif message['type'] == 'graph_updated':
            self.load_viewer(message['note_name'])
        else:
            pass


class NotesViewer(Gtk.Window):
    '''
    Window for displaying notes.
    
    The notes are rendered (markdown->html)
    '''

    def __init__(self, knowledge_model, title):
        super().__init__(title=title)

        self.comm_nexus = None
        self.knowledge_model = knowledge_model

        # webview - displaying html (markdown)
        self.webview = WebKit2.WebView()
        self.add(self.webview)

    @debug_logger
    def register_nexus(self, comm_nexus):
        self.comm_nexus = comm_nexus

    @debug_logger
    def load_note(self, note_name):
        """  """
        #print(note_name)

        self.webview.load_html(
            self.knowledge_model.get_note(note_name, 'html'),
            'file://%s' % os.path.dirname(os.path.realpath(__file__)),
        )

    @debug_logger
    def on_message(self, message):
        if message['type'] == 'preview':
            self.load_note(message['note_name'])
        else:
            pass
