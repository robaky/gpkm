#!/usr/bin/env python3

from other_utils import debug_logger

class Nexus():
    '''
    Communication nexus - receives and distributes
    messages to/from other (peripheral) components.

    Peripheral components should have following methods:

    register_nexus(nexus) : saves the instance of nexus

    on_message(msg) : reacts to message
    '''

    def __init__(self):
        self.channels = {
            'preview_channel':[],
            'select_channel':[],
            'save_channel':[],
        }

    @debug_logger
    def register(self, channel, peripheral):
        '''
        register peripheral objects, assigning
        them to channels
        '''
        self.channels[channel].append(peripheral)

    @debug_logger
    def on_message(self, channel, msg):
        '''
        passes messages to registered peripherals
        '''
        for receipient in self.channels[channel]:
            #print(channel, msg)
            receipient.on_message(msg)
