#!/usr/bin/env python3

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('WebKit2', '4.0')
from gi.repository import Gtk, WebKit2
from gi.repository import Gdk
from gi.repository import GLib
from gi.repository import GObject

from other_utils import debug_logger

class SearchBox(Gtk.Window):
    '''
    A window providing a note searchbox and
    displaying incoming/outgoing connections.

    <how preview / selection work>
    '''

    def __init__(self, knowledge_graph):
        super().__init__(title='searchbox')

        self.knowledge_graph = knowledge_graph
        self.comm_nexus = None

        # grid
        self.grid = Gtk.Grid()
        self.add(self.grid)

        # prep the search bar
        self.prep_searchbar()

        # prep filter buttons
        self.prep_filter_buttons()

        # prep the lists of in/out nodes
        self.in_store, self.in_view = self.setup_list(
                'incoming', (0,1,2,1))
        self.out_store, self.out_view = self.setup_list(
                'outgoing', (2,1,2,1))


    def prep_searchbar(self):
        # container and location
        self.searchbar = Gtk.SearchBar()
        self.grid.attach(self.searchbar, 0, 0, 1, 1)

        # react to events
        self.searchentry = Gtk.SearchEntry()
        self.searchentry.connect("activate", self.on_enter)
        self.searchbar.connect_entry(self.searchentry)
        self.searchbar.add(self.searchentry)

        # show
        self.searchbar.set_search_mode(True)

        # init suggestion list and populate it
        self.prep_search_suggestions_infr()
        self.update_search_suggestions()

    def prep_search_suggestions_infr(self):
        '''
        prepares infrastructure for
        search suggestions in the
        search bar
        '''
        self.search_liststore = Gtk.ListStore(str)
        completion = Gtk.EntryCompletion()
        completion.set_model(self.search_liststore)
        completion.set_text_column(0)
        self.searchentry.set_completion(completion)

    @debug_logger
    def update_search_suggestions(self, filter_dict={}):
        """
        populates the search bar with
        currently available entries
        """
        notes = self.knowledge_graph.get_item_names(**filter_dict)

        self.search_liststore.clear()
        for n in notes:
            self.search_liststore.append([n])


    def prep_filter_buttons(self):
        '''
        prepare toggle buttons which
        control the types of suggested items:
        allows to limit the suggestions to
        notes, zotero or calibre items, or
        combinations of them
        '''
        self.filter_buttons = {}
        counter = 1
        for item_type in ['note', 'zotero', 'calibre']:
            tmp = Gtk.ToggleButton(label=item_type)
            if item_type == 'note':
                tmp.set_active(True)
            self.filter_buttons[item_type] = tmp
            tmp.connect(
                'toggled',
                self.on_filter_button_toggled,
            )
            self.grid.attach(tmp, counter, 0, 1, 1)
            counter += 1

    @debug_logger
    def on_filter_button_toggled(self, button):
        '''
        update the search suggestions based on
        currently active filter buttons
        '''
        allowed_types = [
            k for k,v in 
            self.filter_buttons.items() 
            if v.get_active()
        ]

        self.update_search_suggestions(filter_dict={
            'filter_f':'filter_item_type',
            'filter_args':[allowed_types],
        })

    def setup_list(self, list_name, grid_pos):
        """
        setups a list of incoming or
        outgoing nodes:

        list_name : name to display on GUI

        grid_pos : relative position in grid layout
        (four integers)

        returns: list_store and list_view, so that
        they can be saved and accessed later on for
        updating
        """
        # create base objects
        list_store = Gtk.ListStore(str)
        list_view = Gtk.TreeView(model=list_store)

        # render items and add column
        rend1 = Gtk.CellRendererText()
        col = Gtk.TreeViewColumn(
            list_name,
            rend1,
            text=0,
        )

        list_view.append_column(col)

        # position in the app (layout)
        self.grid.attach(list_view, *grid_pos)

        # communication
        select = list_view.get_selection()
        select.connect(
            "changed",
            self.on_list_selection_changed
        )

        list_view.connect(
            "row-activated",
            self.on_list_activated
        )

        return list_store, list_view


    def on_enter(self, searchentry):
        # send selection signal?
        self.comm_nexus.on_message(
            'select_channel',
            {
                'type':'select',
                'note_name':searchentry.get_text(),
            }
        )

    def on_list_selection_changed(self, selection):
        model, treeiter = selection.get_selected()
        if treeiter is not None:
            self.comm_nexus.on_message(
                'preview_channel',
                {
                    'type':'preview',
                    'note_name':model[treeiter][0],
                }
            )

    def on_list_activated(self, tree_view, path, column):
        model, treeiter = tree_view.get_selection().get_selected()
        if treeiter is not None:
            self.comm_nexus.on_message(
                'select_channel',
                {
                    'type':'select',
                    'note_name':model[treeiter][0],
                }
            )

    @debug_logger
    def update_conns_lists(self, note_name):
        # find incoming and outgoing nodes
        pred = self.knowledge_graph.get_neighbours(note_name, 'in')
        succ = self.knowledge_graph.get_neighbours(note_name, 'out')
        # update UI
        self.set_data_list(self.in_store, pred)
        self.set_data_list(self.out_store, succ)

    def set_data_list(self, list_store, data):
        """
        clears up given list_store and sets
        new data
        """
        list_store.clear()
        # is there a more elegant way?
        for d in data:
            list_store.append([d])

    @debug_logger
    def register_nexus(self, comm_nexus):
        self.comm_nexus = comm_nexus

    @debug_logger
    def on_message(self, message):
        if message['type'] == 'select':
            self.update_conns_lists(message['note_name'])
        else:
            pass
