import sqlite3 as sq
import pandas as pd
import numpy as np
import json
import os
from re import findall
import networkx as nx
import textwrap
import markdown
from config import zotero_path, calibre_path, notes_path

node_cols = ['id', 'type', 'label', 'data']
edge_cols = ['source', 'target', 'type']

item_types = ['note', 'zotero', 'calibre']

html_prefix = '''
    <!DOCTYPE html>
    <html>
    <head>
    <style>
    body {background: #383838; color: #DBDBDB}
    </style>
    </head>
    <body>
    '''

html_suffix = '''
    </body>
    </html>
    '''



def read_zotero():
    # read main zotero data    
    con = sq.connect(os.path.join(zotero_path, 'zotero.sqlite'))    
    
    df_main = pd.read_sql_query("SELECT itemID, key, itemTypeID from items", con)    
    df_item_attachments = pd.read_sql_query("SELECT itemID, parentItemID from itemAttachments", con)    
    
    con.close()    
    
    # read better bibtex data    
    con = sq.connect(os.path.join(zotero_path, 'better-bibtex.sqlite'))    
    cursor = con.execute("SELECT data FROM 'better-bibtex' WHERE name IS 'better-bibtex.citekey'")    
    
    out = cursor.fetchall()    
    data = json.loads(out[0][0])    
    df_cite = pd.DataFrame(data['data'])    
    con.close()  
    
    # prepare dataframes for extraction of citation-path_to_file data    
    citables = df_cite['itemID'].to_list()
    
    df_main = df_main.set_index('itemID')
    df_item_attachments = df_item_attachments.set_index('parentItemID')
    
    
    def find_linked_files(key):
        """ find folders and linked files for cited items """
        try:
            try:
                connected = df_item_attachments.loc[key]['itemID'].to_list()
            except AttributeError:
                connected = [df_item_attachments.loc[key]['itemID']]
        except KeyError as e:
            return np.nan
        
        folder_candidates = df_main.loc[connected]['key'].to_list()
        out = []
        for cand in folder_candidates:
            path_tmp = os.path.join(zotero_path, 'storage', cand)
            out.extend([os.path.join(path_tmp, f) for f in os.listdir(path_tmp) if not f.startswith('.')])
        return out
    
    # find paths to files for items
    df_cite['path_to_file'] = df_cite['itemID'].map(lambda key: find_linked_files(key))
    
    # reformat data frame
    #df_cite = df_cite[['citekey', 'path_to_file']].copy()
    df_cite['id'] = df_cite['citekey']
    df_cite['label'] = df_cite['citekey']
    df_cite['type'] = 'zotero'
    
    df_nodes = df_cite[['id', 'label', 'type', 'path_to_file']].copy()
    
    return df_nodes


def read_calibre():
    """ extract books from calibre """
    nodes = []
    for folder_author in os.listdir(calibre_path):
        try:
            for folder_book in os.listdir(os.path.join(calibre_path, folder_author)):    
                book_path = os.path.join(calibre_path, folder_author, folder_book)
    
                nodes.append({    
                    'id':folder_book,    
                    'type':'calibre',     
                    'label':folder_book,     
                    'path_to_file':book_path,    
                })
        except NotADirectoryError:
            pass
        
    return pd.DataFrame(nodes)


def find_references(text_string):
    return findall(r'\[\[(.+?)\]\]', text_string)


def read_notes():
    """ extract notes and references """
    def read_note_md(note_path):
        with open(note_path) as f:
            return f.readlines()

    notes = []
    edges = []

    for note in os.listdir(notes_path):
        if note.startswith('.'):
            continue
        if os.path.isdir(os.path.join(notes_path, note)):
            continue

        node_data = read_note_md(os.path.join(notes_path, note))

        notes.append({
            'id':note[:-3],
            'type':'note', 
            'label':note[:-3], 
            'raw_txt':node_data,
        })

        for link in find_references(''.join(node_data)):
            edges.append({
                'source':note[:-3],
                'target':link,
                'type':'note_ref',
            })

    notes_n, edges_n = pd.DataFrame(notes), pd.DataFrame(edges)
    notes_n['markdown'] = notes_n['raw_txt'].map(lambda x: ''.join(x))

    def render_html(markdown_txt):
        return html_prefix + markdown.markdown(markdown_txt) + html_suffix

    notes_n['html'] = notes_n['markdown'].map(render_html)

    return notes_n, edges_n

def prep_graph(nodes_z, nodes_c, nodes_n, edges_n):
    """  """
    nodes_df = pd.concat([nodes_z, nodes_c, nodes_n])

    assert nodes_df['id'].isna().sum() == 0

    nodes_df = nodes_df.set_index('id', drop=False)
    nodes_df['text_len'] = 0
    nodes_df['node_label'] = ''

    # select notes and prep fields
    is_note = nodes_df['type'] == 'note'
    nodes_df.loc[is_note, 'text_len'] = nodes_df.loc[is_note]['raw_txt'].map(
        lambda x: len(''.join(x)))
    nodes_df.loc[is_note, 'node_label'] = nodes_df[is_note]['label'].map(
        break_label)

    # select data to build graph
    # selected_cols = ['id', 'type', 'text_len', 'node_label', , ]
    tmp_nodes = nodes_df.to_dict(orient='records')
    tmp_edges = [(row['source'], row['target']) for
        idx,row in edges_n.iterrows()]

    # create graph
    graph = nx.DiGraph()
    graph.add_nodes_from((n['id'], n) for n in tmp_nodes)
    graph.add_edges_from(tmp_edges)

    return graph



def break_label(label):
    return '<br>'.join(textwrap.wrap(label, width=20, break_long_words=False))


def apply_graph_style_1(graph, cited_items):
    """ show all notes and referenced items """
    subset_nodes = [n for n,v in graph.nodes(data=True) if v['type'] == 'note']
    return graph.subgraph(subset_nodes + cited_items)
