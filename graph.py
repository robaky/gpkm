#! /usr/bin/env python3

import os
import numpy as np
import matplotlib as mpl
mpl.use('GTK3Agg')  # or 'GTK3Cairo'
import matplotlib.pyplot as plt
from matplotlib.backends.backend_gtk3agg import (
    FigureCanvasGTK3Agg as FigureCanvas)

from matplotlib.backends.backend_gtk3 import (
    NavigationToolbar2GTK3 as NavigationToolbar)

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk

import networkx as nx
import netgraph

from other_utils import debug_logger
from config import notes_path

import json


class Graph(Gtk.Window):
    '''
    Graph showing notes, citations from notes
    and items from zotero and calibre.

    Clicking on items selects them (affecting 
    other components), while hovering triggers
    preview.

    <what options for control of what is shown>
    '''

    def __init__(self, knowledge_model):
        # prep window
        super().__init__(title='graph')
        self.zoom_scale = 1.5
        self.node_shape_map = {
            'note':'o',
            'zotero':'^',
            'calibre':'h',
        }
        self.edge_colors = {
            'default':'lavender',
            'in':'red',
            'out':'yellow'
        }

        self.knowledge_model = knowledge_model
        self.comm_nexus = None

        self.prep_figure()
        self.plot_figure()

        self.fig.canvas.mpl_connect('motion_notify_event', self.on_hover)
        self.fig.canvas.mpl_connect('button_press_event', self.on_click)
        self.fig.canvas.mpl_connect('scroll_event', self.on_scroll)

        # need to handle both press and release
        # to use ctrl as a modifier while mouse clicking
        self.connect("button_release_event", self.button_release_callback)

        # a Gtk.DrawingArea
        self.canvas = FigureCanvas(self.fig)
        self.canvas.set_size_request(800, 600)
        self.add(self.canvas)

        # Create toolbar
        self.hb = Gtk.HeaderBar()
        #button_layout_mode = Gtk.Button('layout')
        button_save = Gtk.Button('save layout')
        button_load = Gtk.Button('load layout')

        #button_layout_mode.connect('clicked', self.open_mode_dialog)
        button_save.connect('clicked', self.open_layout_save_dialog)
        button_load.connect('clicked', self.open_layout_load_dialog)

        button_layout_mode = self.prep_mode_combobox()

        self.hb.add(button_layout_mode)
        self.hb.add(button_save)
        self.hb.add(button_load)

        self.set_titlebar(self.hb)

        self.on_click_coors = None
        self.to_select_node = None

        self.highlited_edges = []


    def prep_mode_combobox(self):
        modes = ['netgraph', 'manual']

        mode_store = Gtk.ListStore(str)

        for m in modes:
            mode_store.append([m])

        self.mode_combo = Gtk.ComboBox.new_with_model(mode_store)
        self.mode_combo.connect("changed", self.change_mode)
        renderer_text = Gtk.CellRendererText()
        self.mode_combo.pack_start(renderer_text, True)
        self.mode_combo.add_attribute(renderer_text, "text", 0)

        # sets first item in the store list as active selection
        self.mode_combo.set_active(0)

        return self.mode_combo


    def open_layout_save_dialog(self, event):
        '''  '''

        dialog = Gtk.FileChooserDialog(
            title="Please choose a file",
            parent=self,
            action=Gtk.FileChooserAction.SAVE
        )

        dialog.set_current_folder(
            os.path.join(notes_path, 'saved_layouts')
        )

        dialog.add_buttons(
            Gtk.STOCK_CANCEL,
            Gtk.ResponseType.CANCEL,
            Gtk.STOCK_OPEN,
            Gtk.ResponseType.OK,
        )

        self.add_filters(dialog)

        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            self.save_layout(dialog.get_filename())
        elif response == Gtk.ResponseType.CANCEL:
            pass

        dialog.destroy()


    def open_layout_load_dialog(self, event):
        '''  '''
        dialog = Gtk.FileChooserDialog(
            title="Please choose a file",
            parent=self, 
            action=Gtk.FileChooserAction.OPEN
        )

        dialog.set_current_folder(
            os.path.join(notes_path, 'saved_layouts')
        )

        dialog.add_buttons(
            Gtk.STOCK_CANCEL,
            Gtk.ResponseType.CANCEL,
            Gtk.STOCK_OPEN,
            Gtk.ResponseType.OK,
        )

        self.add_filters(dialog)

        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            self.load_and_apply_layout(dialog.get_filename())
            # switch to manual
            self.mode_combo.set_active(1)
        elif response == Gtk.ResponseType.CANCEL:
            pass

        dialog.destroy()


    def save_layout(self, path):
        ''' save layout to selected file 

            saves only positions from self.netgraph, not
            the full graph stored in the self.knowledge_model,
            i.e. saves only positions of nodes and other items
            referenced by them
        '''
        pos = {k:tuple(v) for k,v in self.netgraph.node_positions.items()}

        with open(path, "w") as outfile:
            json.dump(pos, outfile)


    def load_and_apply_layout(self, path):
        ''' '''
        # load position data
        with open(path, "r") as infile:
            pos = json.load(infile)

        # apply the positions and redraw
        self.plot_figure(node_positions=pos)


    def add_filters(self, dialog):
        filter_text = Gtk.FileFilter()
        filter_text.set_name("Text files")
        filter_text.add_mime_type("text/plain")
        dialog.add_filter(filter_text)

        filter_py = Gtk.FileFilter()
        filter_py.set_name("Python files")
        filter_py.add_mime_type("text/x-python")
        dialog.add_filter(filter_py)

        filter_any = Gtk.FileFilter()
        filter_any.set_name("Any files")
        filter_any.add_pattern("*")
        dialog.add_filter(filter_any)


    def change_mode(self, combo):
        ''' if mode changed from manual
            to any other then force redraw
            with the new layout

            currently the only other is
            netgraph's default
        '''
        tree_iter = combo.get_active_iter()

        if tree_iter is not None:
            model = combo.get_model()
            mode = model[tree_iter][0]

            if mode != 'manual':
                self.plot_figure()

    def prep_figure(self):
        self.fig, self.ax = plt.subplots()

    def get_node_shapes(self, tmp_graph):
        return {
            n[0]:self.node_shape_map[n[1]['type']] for
            n in tmp_graph.nodes(data=True)
        }

    def get_node_sizes(self, tmp_graph):
        '''  '''
        sizes = {
            n[0]:np.clip((np.log(n[1])-2), 4, 15) for n in
            tmp_graph.nodes.data('text_len')
        }

        return sizes

    def plot_figure(self, node_positions=None):
        self.ax.clear() # clear previous data
        self.ax.axis('off') # remove axis

        # select subgraph to plot:
        # only notes and items connected to them
        #
        # flatten edges to nodes
        edge_nodes = [
            item for sublist in 
            self.knowledge_model.graph.edges()
            for item in sublist
        ]
        # combine nodes from edges with all note nodes
        tmp_nodes = self.knowledge_model.filter_item_type('note')
        tmp_nodes = list(set(tmp_nodes) | set(edge_nodes))

        tmp_graph = self.knowledge_model.graph.subgraph(tmp_nodes)

        #
        # prep style arguments
        #
        # node shape depends on it's type (note, zotero, calire)
        nodes_shapes = self.get_node_shapes(tmp_graph)

        # size of note nodes depends on amount of text
        nodes_sizes = self.get_node_sizes(tmp_graph)

        # this handles situations where new nodes
        # are added to the graph
        # (so the saved layout didn't have them)
        #
        # generates some positions first, then
        # overrides them if node_positions were
        # supplied - the new nodes keep the initially
        # generated positions as there is nothig to
        # override them with
        default_pos = netgraph.get_fruchterman_reingold_layout(
            edges=list(tmp_graph.edges()),
            nodes=list(tmp_graph.nodes()),
        )

        if node_positions is None:
            pass
        else:
            default_pos.update(node_positions)

        self.netgraph = netgraph.InteractiveGraph(
            tmp_graph,
            node_layout=default_pos,
            node_shape=nodes_shapes,
            node_size=nodes_sizes,
            edge_color=self.edge_colors['default'],
            arrows=True,
            ax=self.ax,
        )

        # prepare point annotations to show
        self.annot = self.ax.annotate(
            "", xy=(0,0), xytext=(10,10),
            textcoords="offset points",
            bbox=dict(boxstyle="round", fc="w"),
        )

        # dark background
        self.fig.patch.set_facecolor('#282828')
        self.ax.set_facecolor('#282828')
        self.fig.canvas.draw()

    def on_graph_updated(self):
        cur_pos = self.netgraph.node_positions
        self.plot_figure(cur_pos)

    def button_release_callback(self, widget, event, data=None):
        '''
        reacts to clicks

        works together with on_click, to work around
        limitations in data access: can't access
        the keyboard down buttons and mouse click 
        from the same event
        '''
        # if ctrl held, clicks navigate around the plot
        if event.get_state() & Gdk.ModifierType.CONTROL_MASK:
            self.pan_to_center(self.on_click_coors)

        # otherwise select node
        elif not self.to_select_node is None:
            self.send_select_signal(self.to_select_node)
            self.to_select_node = None

        self.fig.canvas.draw_idle()

    def on_click(self, event):
        self.on_click_coors = (event.xdata, event.ydata)

        for node, artist in self.netgraph.node_artists.items():
            cont, ind = artist.contains(event)
            if cont:
                self.to_select_node = node
                break

    def on_hover(self, event):
        '''
        update the plot when hovering over nodes

        netgraph does highlighting by default. What
        I've added is:
        - showing/hiding annotations
        - coloring in/out edges

        TODO: reactions to hovering are not very reliable;
        sometimes they disappear while the mouse pointer
        is over the marker
        '''
        vis = self.annot.get_visible()
        if event.inaxes == self.ax:
            for node, artist in self.netgraph.node_artists.items():
                cont, ind = artist.contains(event)

                # breaks within the if ... else
                # assume that there can only be one
                # node artist being hovered over
                # (which is quite natural)

                if cont and vis:
                    # moving within the node
                    break

                elif cont and not vis:
                    # entering the node
                    #
                    # show annotation
                    self.update_annot(node)
                    self.annot.set_visible(True)

                    # color in/out edges
                    # to highlight direction
                    for d in ['in', 'out']:
                        c = self.edge_colors[d]
                        for e in self.knowledge_model.get_edges(node,d):
                            self.netgraph.edge_artists[e].set_color(c)
                            self.highlited_edges.append(
                                self.netgraph.edge_artists[e]
                            )

                    # housekeeping
                    self.send_preview_signal(node)

                    break

                elif not cont and vis:
                    # leaving the node
                    #
                    # hide annotation
                    self.annot.set_visible(False)

                    # reset edges color
                    for a in self.highlited_edges:
                        a.set_color('lavender')
                    self.highlited_edges = [] # clear the list

                    break

                else:
                    pass


    def on_scroll(self, event):
        xdata, ydata = event.xdata, event.ydata
        if event.button == 'up':
            zoom_in = True
        else:
            zoom_in = False

        # get the current x and y limits
        cur_xlim = self.ax.get_xlim()
        cur_ylim = self.ax.get_ylim()
        cur_xrange = (cur_xlim[1] - cur_xlim[0])*.5
        cur_yrange = (cur_ylim[1] - cur_ylim[0])*.5

        if event.button == 'up':
            # deal with zoom in
            scale_factor = 1/self.zoom_scale
        elif event.button == 'down':
            # deal with zoom out
            scale_factor = self.zoom_scale
        else:
            # deal with something that should never happen
            scale_factor = 1
            print(event.button)

        # set new limits
        self.ax.set_xlim([xdata - (xdata-cur_xlim[0]) / scale_factor,
                     xdata + (cur_xlim[1]-xdata) / scale_factor])
        self.ax.set_ylim([ydata - (ydata-cur_ylim[0]) / scale_factor,
                     ydata + (cur_ylim[1]-ydata) / scale_factor])

        self.fig.canvas.draw_idle()


    def update_annot(self, node):
        # hover
        pos = self.netgraph.node_positions[node]
        self.annot.xy = pos
        text = node
        self.annot.set_text(text)

        self.annot.set(
            color='white',
            backgroundcolor='#282828',
            fontsize='large',
            fontstyle='oblique',
        )
        self.annot.get_bbox_patch().set(
            edgecolor='#282828'
        )


    def send_preview_signal(self, note_name):
        self.comm_nexus.on_message(
            'preview_channel',
            {
                'type':'preview',
                'note_name':note_name,
            }
        )

    def send_select_signal(self, note_name):
        self.comm_nexus.on_message(
            'select_channel',
            {
                'type':'select',
                'note_name':note_name,
            }
        )

    def pan_to_node(self, node):
        '''
        center the view on the node
        while preserving the zoom lvl
        '''
        node_pos = self.netgraph.node_positions[node]
        self.pan_to_center(node_pos)


    def pan_to_center(self, pos):
        '''
        center the view on given position
        while preserving the zoom lvl
        '''
        xlim = self.ax.get_xlim()
        ylim = self.ax.get_ylim()

        x_size = xlim[1] - xlim[0]
        y_size = ylim[1] - ylim[0]

        new_xlim = (pos[0]-x_size/2, pos[0]+x_size/2)
        new_ylim = (pos[1]-y_size/2, pos[1]+y_size/2)

        self.ax.set_xlim(new_xlim)
        self.ax.set_ylim(new_ylim)


    @debug_logger
    def on_select(self, node_name):
        self.pan_to_node(node_name)
        self.fig.canvas.draw_idle()

    @debug_logger
    def on_preview(self, node_name):
        pass

    @debug_logger
    def register_nexus(self, comm_nexus):
        self.comm_nexus = comm_nexus

    @debug_logger
    def on_message(self, message):
        if message['type'] == 'select':
            self.on_select(message['note_name'])
        elif message['type'] == 'preview':
            self.on_preview(message['note_name'])
        elif message['type'] == 'graph_updated':
            self.on_graph_updated()
        else:
            pass

