#!/usr/bin/env python3

import os
import markdown

from data_utils import read_notes, read_calibre, read_zotero, prep_graph, notes_path, find_references, html_prefix, html_suffix
from other_utils import debug_logger

class KnowledgeModel():
    def __init__(self):
        # load data and create a graph
        self.comm_nexus = None

        nodes_c = read_calibre()
        nodes_z = read_zotero()
        nodes_n, edges_n = read_notes()

        self.graph = prep_graph(nodes_z, nodes_c, nodes_n, edges_n)


    def get_item_names(self, filter_f=None, filter_args=None):
        '''
        returns names of all items:
        notes, zotero and calibre
        
        filter_dict should follow schema:
        {'filter_func_name':args}
        where args is a list of arguments
        '''

        if filter_f is None:
            return self.graph.nodes
        else:
            method = getattr(self, filter_f)
            return method(*filter_args)

    def filter_item_type(self, allowed_types):
        ''' returns items of specified types '''
        return [
            n[0] for 
            n in 
            self.graph.nodes(data=True) 
            if n[1]['type'] in allowed_types
        ]

    def get_note(self, note_name, note_format):
        return self.graph.nodes[note_name][note_format]

    def get_edges(self, note_name, direction):
        if direction == 'in':
            return self.graph.in_edges(note_name)
        else:
            return self.graph.out_edges(note_name)

    def get_neighbours(self, note_name, direction):
        if direction == 'in':
            return self.graph.predecessors(note_name)
        else:
            return self.graph.successors(note_name)

    @debug_logger
    def register_nexus(self, comm_nexus):
        self.comm_nexus = comm_nexus

    @debug_logger
    def on_message(self, message):
        if message['type'] == 'save_note':
            self.on_edit(message['note_name'], message['new_txt'])
        else:
            pass

    def on_edit(self, note_name, new_txt):
        '''
        react to note edit
        '''
        md = ''.join(new_txt)
        # update data in the graph
        self.graph.nodes[note_name].update({
            'raw_txt':new_txt,
            'markdown':md,
            'html':html_prefix + markdown.markdown(md) + html_suffix,
            'text_len':len(''.join(new_txt)),
#            'node_label':None,
        })

        # TODO what if the note is renamed?
        # TODO update note label?
        # TODO update note id?

        # update outgoing conns
        # delete all existing outgoing conns
        # need to cast to list to avoid the
        # "RuntimeError: dictionary changed size during iteration"
        e_to_remove = list(self.graph.edges(note_name))
        self.graph.remove_edges_from(e_to_remove)

        # find and save new ones
        new_conns = []
        for target_node in find_references(md):
            new_conns.append((note_name, target_node))

        self.graph.add_edges_from(new_conns)

        # update the file
        f_path = os.path.join(notes_path, note_name+'.md')
        with open(f_path, 'w') as f:
            f.writelines(new_txt)

        self.comm_nexus.on_message(
            'save_channel',
            {
                'type':'graph_updated',
                'note_name':note_name,
            },
        )
