import logging

logging.basicConfig(
    filename='log.log',
    level=logging.DEBUG
)

def debug_logger(method):
    '''
    when a decorated method is called
    logs names of the parent object and the method
    as well as values of input and output values
    '''

    def wrapper(self, *args, **kwargs):
        print(self, method)
        ret = method(self, *args, **kwargs)
        logging.debug(f'called {self}.{method.__name__} with:\n{args, kwargs}\nreturned {ret}')
        return ret

    return wrapper

